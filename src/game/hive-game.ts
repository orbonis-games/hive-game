import { Game } from "libs/core-game/src/game/game";
import { Board, Tile } from "./renderables/board";

export class HiveGame extends Game {
    public async init(canvas: HTMLCanvasElement): Promise<void> {
        super.init(canvas, []);
    }

    public prerender(delta: number): void {
        super.prerender(delta);
    }
    
    protected async initRenderables(): Promise<void> {
        if (this.app) {
            const board = new Board("board", this.app);
            board.onTileSelect.connect((tile) => this.tileSelected(tile));
            await super.initRenderables(
                board
            );
        }
    }

    protected tileSelected(tile: Tile): void {
        console.log(tile);
        tile.color = 0xFFFF00;
    }
}