import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { Graphics, Point } from "pixi.js";
import { Signal } from "typed-signals";

export interface Tile {
    graphic: Graphics;
    position: Point;
    selected: boolean;
    color: number;
}

export class Board extends Renderable {
    public onTileSelect: Signal<(tile: Tile) => void> = new Signal();

    protected hexagonRadius = 20;
    protected hexagonHeight = this.hexagonRadius * Math.sqrt(3);
    protected tiles: Tile[] = [];

    public async init(): Promise<void> {

        this.tiles = [];

        for (let y = 0; y < 1000 / this.hexagonHeight; y++) {
            for (let x = 0; x < 1000 / this.hexagonRadius; x++) {
                const tile: Tile = { graphic: new Graphics(), position: new Point(x, y), selected: false, color: 0xFFFFFF };
                tile.graphic.position = new Point(x * (this.hexagonRadius * 1.5), (y * this.hexagonHeight) + ((x % 2 === 1) ? this.hexagonHeight / 2 : 0));
                tile.graphic.cacheAsBitmap = true;
                tile.graphic.interactive = true;
                tile.graphic.buttonMode = true;
                tile.graphic.addListener("pointerover", () => {
                    tile.selected = true;
                    this.redrawTile(tile);
                });
                tile.graphic.addListener("pointerout", () => {
                    tile.selected = false;
                    this.redrawTile(tile);
                });
                tile.graphic.addListener("pointerup", () => {
                    this.onTileSelect.emit(tile);
                    this.redrawTile(tile);
                });
                this.redrawTile(tile);
                this.tiles.push(tile);
                this.app.stage.addChildAt(tile.graphic, 0);
            }
        }
    }

    public render(delta: number): void {
        
    }

    public getColliders(): CollidableShape[] {
        return [];
    }

    private redrawTile(tile: Tile): void {
        tile.graphic.cacheAsBitmap = false;
        tile.graphic.clear();
        tile.graphic.lineStyle(2, (tile.selected) ? 0xFFCCCC : 0xCCCCCC);
        tile.graphic.beginFill(tile.color, 0.1);
        tile.graphic.drawPolygon([
            -this.hexagonRadius, 0,
            (-this.hexagonRadius/2), (this.hexagonHeight/2),
            (this.hexagonRadius/2), (this.hexagonHeight/2),
            this.hexagonRadius, 0,
            (this.hexagonRadius/2), (-this.hexagonHeight/2),
            (-this.hexagonRadius/2), (-this.hexagonHeight/2)
        ]);
        tile.graphic.endFill();
        tile.graphic.cacheAsBitmap = true;
        if (tile.selected) {
            tile.graphic.parent?.addChildAt(tile.graphic, tile.graphic.parent.children.length);
        }
    }
}